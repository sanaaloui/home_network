import React, { Fragment, useState, useEffect } from "react";
import { withRouter, Link } from "react-router-dom";
import propTypes from "prop-types";
import { connect } from "react-redux";
import { createProfile, getCurrentProfile } from "../../actions/profileActions";
import "../../CSS/createProfile.css";

const EditProfile = ({
	profile: { profile, loading },
	createProfile,
	getCurrentProfile,
	history
}) => {
	const [formData, setFormData] = useState({
		status: "",
		company: "",
		phone: "",
		website: "",
		location: "",

		bio: "",
		twitter: "",
		facebook: "",
		linkedin: "",
		instagram: ""
	});
	const [displaySocialInputs, toggleSocialInputs] = useState(false);

	useEffect(() => {
		setFormData({
			status: loading || !profile.status ? "" : profile.status,
			company: loading || !profile.company ? "" : profile.company,
			phone: loading || !profile.phone ? "" : profile.phone,
			website: loading || !profile.website ? "" : profile.website,
			location: loading || !profile.location ? "" : profile.location,

			bio: loading || !profile.bio ? "" : profile.bio,
			twitter: loading || !profile.social ? "" : profile.social.twitter,
			facebook: loading || !profile.social ? "" : profile.social.facebook,
			linkedin: loading || !profile.social ? "" : profile.social.linkedin,
			instagram: loading || !profile.social ? "" : profile.social.instagram
		});
	}, [loading, profile]);

	const {
		company,
		location,
		status,
		phone,
		website,
		bio,
		twitter,
		facebook,
		linkedin,
		instagram
	} = formData;
	const onChange = e =>
		setFormData({ ...formData, [e.target.name]: e.target.value });
	const onSubmit = e => {
		e.preventDefault();
		createProfile(formData, history, true);
	};

	return (
		<Fragment>
			<h1 className="large ">Modifier votre profil</h1>
			<p className="lead">
				<i className="fas fa-user" /> Ajouter des modifications à votre profil
			</p>
			<form className="form" onSubmit={e => onSubmit(e)}>
				<div className="form-group">
					<select
						className="input"
						name="status"
						value={status}
						onChange={e => onChange(e)}>
						<option value="0">indiquer votre statut professionnel</option>
						<option value="Propriétaire">Propriétaire</option>
						<option value="Responsable Prospection">
							Responsable Prospection{" "}
						</option>
						<option value="Other">Autre</option>
					</select>
				</div>
				<div className="form-group">
					<input
						className="input"
						type="text"
						placeholder="Agence"
						name="company"
						value={company}
						onChange={e => onChange(e)}
					/>
				</div>
				<div className="form-group">
					<input
						className="input"
						type="text"
						placeholder="Téléphone"
						name="phone"
						value={phone}
						onChange={e => onChange(e)}
					/>
				</div>
				<div className="form-group">
					<input
						className="input"
						type="text"
						placeholder="Ajouter un site Web"
						name="website"
						value={website}
						onChange={e => onChange(e)}
					/>
				</div>
				<div className="form-group">
					<input
						className="input"
						type="text"
						placeholder="Région"
						name="location"
						value={location}
						onChange={e => onChange(e)}
					/>
				</div>

				<div className="form-group">
					<textarea
						className="input"
						placeholder="Présenter votre agence"
						name="bio"
						value={bio}
						onChange={e => onChange(e)}
					/>
				</div>
				<div>
					<button
						onClick={() => toggleSocialInputs(!displaySocialInputs)}
						type="button"
						className="btn btn-light">
						Ajouter des liens vers vos réseaux sociaux
					</button>
				</div>
				{displaySocialInputs && (
					<Fragment>
						<div className="form-group social-input">
							<i className="fab fa-twitter fa-2x" />
							<input
								className="input"
								type="text"
								placeholder="URL du Twitter "
								name="twitter"
								value={twitter}
								onChange={e => onChange(e)}
							/>
						</div>
						<div className="form-group social-input">
							<i className="fab fa-facebook fa-2x" />
							<input
								className="input"
								type="text"
								placeholder="URL d'un compte Facebook "
								name="facebook"
								value={facebook}
								onChange={e => onChange(e)}
							/>
						</div>

						<div className="form-group social-input">
							<i className="fab fa-linkedin fa-2x" />
							<input
								className="input"
								type="text"
								placeholder="URL de votre Profil Linkedin "
								name="linkedin"
								value={linkedin}
								onChange={e => onChange(e)}
							/>
						</div>
						<div className="form-group social-input">
							<i className="fab fa-instagram fa-2x" />
							<input
								className="input"
								type="text"
								placeholder="URL d'Instagram "
								name="instagram"
								value={instagram}
								onChange={e => onChange(e)}
							/>
						</div>
					</Fragment>
				)}
				<input type="submit" className="btn " />
				<Link className="btn btn-light my-1" to="/dashboard">
					Go Back
				</Link>
			</form>
		</Fragment>
	);
};

EditProfile.propTypes = {
	createProfile: propTypes.func.isRequired,
	profile: propTypes.object.isRequired,
	getCurrentProfile: propTypes.func.isRequired
};

const mapStateToProps = state => ({
	profile: state.profile
});

export default connect(mapStateToProps, { createProfile, getCurrentProfile })(
	withRouter(EditProfile)
);
