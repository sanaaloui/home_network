import React, { Component, Fragment } from "react";
import { Link, withRouter } from "react-router-dom";
import propTypes from "prop-types";
import { connect } from "react-redux";
import { createProfile } from "../../actions/profileActions";
import "../../CSS/createProfile.css";

class CreateProfile extends Component {
	constructor() {
		super();
		this.state = {
			displaySocialInputs: false,
			status: "",
			company: "",
			phone: "",
			website: "",
			location: "",
			bio: "",
			instagram: "",
			facebook: "",
			linkedin: "",
			twitter: ""
		};
	}

	// componentDidMount = () => {
	// 	this.props.history.push("/dashboard");
	// };

	onSubmit = async e => {
		e.preventDefault();
		const formData = {
			status: this.state.status,
			company: this.state.company,
			phone: this.state.phone,
			bio: this.state.bio,
			website: this.state.website,
			location: this.state.location,
			instagram: this.state.instagram,
			facebook: this.state.facebook,
			linkedin: this.state.linkedin,
			twitter: this.state.twitter
		};
		this.props.createProfile(formData, this.props.history);
		// console.log(userData)
	};

	onChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	ToggleButton() {
		this.setState(currentState => ({
			displaySocialInputs: !currentState.displaySocialInputs
		}));
	}

	render() {
		return (
			<Fragment>
				<h1 className="large ">Créer votre profil</h1>
				<p className="lead">
					<i className="fas fa-user"></i> Entrez les renseignements sur votre
					profil.
				</p>

				<form className="form" onSubmit={e => this.onSubmit(e)}>
					<div
						className="form-group"
						value={this.state.status}
						onChange={e => this.onChange(e)}>
						<select className="input" name="status">
							<option value="0">indiquer votre statut professionnel</option>
							<option value="Propriétairer">Propriétaire</option>
							<option value="Responsable Prospection">
								Responsable Prospection{" "}
							</option>
							<option value="Other">Autre</option>
						</select>
					</div>
					<div className="form-group">
						<input
							className="input"
							type="text"
							placeholder="Agence"
							name="company"
							value={this.state.company}
							onChange={e => this.onChange(e)}
						/>
					</div>
					<div className="form-group">
						<input
							className="input"
							type="text"
							placeholder="Téléphone"
							name="phone"
							value={this.state.phone}
							onChange={e => this.onChange(e)}
						/>
					</div>
					<div className="form-group">
						<input
							className="input"
							type="text"
							placeholder="Ajouter un site Web"
							name="website"
							value={this.state.website}
							onChange={e => this.onChange(e)}
						/>
					</div>
					<div className="form-group">
						<input
							className="input"
							type="text"
							placeholder="Région"
							name="location"
							value={this.state.location}
							onChange={e => this.onChange(e)}
						/>
					</div>

					<div className="form-group">
						<textarea
							className="input"
							placeholder="Présenter votre agence"
							name="bio"
							value={this.state.bio}
							onChange={e => this.onChange(e)}></textarea>
					</div>

					<div>
						<button
							onClick={() => this.ToggleButton()}
							value={!this.state.displaySocialInputs}
							type="button"
							className="btn btn-light">
							Ajouter des liens vers vos réseaux sociaux
						</button>
					</div>
					{this.state.displaySocialInputs && (
						<Fragment>
							<div className="form-group social-input">
								<i className="fab fa-twitter fa-2x"></i>
								<input
									className="input"
									type="text"
									placeholder="URL du Twitter "
									name="twitter"
									value={this.state.twitter}
									onChange={e => this.onChange(e)}
								/>
							</div>

							<div className="form-group social-input">
								<i className="fab fa-facebook fa-2x"></i>
								<input
									className="input"
									type="text"
									placeholder="URL d'un compte Facebook"
									name="facebook"
									value={this.state.facebook}
									onChange={e => this.onChange(e)}
								/>
							</div>

							<div className="form-group social-input">
								<i className="fab fa-linkedin fa-2x"></i>
								<input
									className="input"
									type="text"
									placeholder="URL de votre Profil Linkedin "
									name="linkedin"
									value={this.state.linkedin}
									onChange={e => this.onChange(e)}
								/>
							</div>

							<div className="form-group social-input">
								<i className="fab fa-instagram fa-2x"></i>
								<input
									className="input"
									type="text"
									placeholder=" URL d'Instagram "
									name="instagram"
									value={this.state.instagram}
									onChange={e => this.onChange(e)}
								/>
							</div>
						</Fragment>
					)}

					<input type="submit" className="btn " />
					<Link className="btn btn-light my-1" to="/dashboard">
						Go Back
					</Link>
				</form>
			</Fragment>
		);
	}
}

CreateProfile.propTypes = {
	createProfile: propTypes.func.isRequired
};

export default connect(null, { createProfile })(withRouter(CreateProfile));
