import React from "react";
import "../CSS/homePage.css";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import propTypes from "prop-types";
import Carousel from "./carousel/Carousel";

const Home = ({ isAuthenticated }) => {
	if (isAuthenticated) {
		return <Redirect to="/dashboard" />;
	}
	return (
		<section className="cont">
			<div className="landing-inner">
				{/* <h1 className="x-large">Home Connector</h1> */}
				<Carousel />
			</div>
			<section className="section section-md bg-default section-shadow-bottom">
				<div className="container">
					<div className="row row-50">
						<div className="col-lg-7 col-md-6">
							<h3>Pourquoi nous choisir ? </h3>
							<div className="services">
								<div className="unit align-items-end unit-spacing-md">
									<div className="unit-body">
										<h4 className="services-title ">Excellente réputation</h4>
									</div>
								</div>
								<p className="services-text">
									Home Connector une excellente réputation sur le marché et dans
									les communautés que nous desservons. Notre équipe vit dans les
									communautés que nous desservons, ce qui nous rend pleinement
									motivés et dévoués.
								</p>
							</div>
							<div className="services">
								<div className="unit align-items-end unit-spacing-md">
									<div className="unit-body">
										<h4 className="services-title ">Nous sommes ingénieux</h4>
									</div>
								</div>
								<p className="services-text">
									Une plateforme multi-services avec des milliers de contacts de
									haut niveau.
								</p>
							</div>
							<div className="services">
								<div className="unit align-items-end unit-spacing-md">
									<div className="unit-body">
										<h4 className="services-title ">INNOVANT</h4>
									</div>
								</div>
								<p className="services-text">
									Le digital au service de la relation entre les professionnels
									de l’immobilier: Centralisation des demandes et des recherches
								</p>
							</div>
						</div>
						<div className="col-lg-5 col-md-6">
							<h3>Nos avantages</h3>
							<ul className="list-marked heading-4">
								<li>Travail d'équipe</li>
								<li>Connaissance du marché</li>
								<li>Flexibilité </li>
								<li>Conseils</li>
								<li>Négociation</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
		</section>
	);
};
Home.propTypes = {
  isAuthenticated: propTypes.bool
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps)(Home);
