import React, { Component } from "react";
import "../../CSS/homePage.css";
import { Carousel } from "react-responsive-carousel";

// // general styles
import "react-responsive-carousel/lib/styles/main.css";

// // carousel styles
import "react-responsive-carousel/lib/styles/carousel.css";

// import slide1 from "../carousel/slide1.png";
import slide2 from "../carousel/slide2.png";
import slide3 from "../carousel/slide3.png";

class DemoCarousel extends Component {
	render() {
		return (
			<Carousel
				showThumbs={false}
				showArrows={true}
				infiniteLoop={true}
				autoPlay={true}>
				{/* <div>
					<img className="img_carousel" src={slide1} alt="" />{" "}
				</div> */}
				<div>
					<img className="img_carousel" src={slide2} alt="" />
				</div>

				<div>
					<img className="img_carousel" src={slide3} alt="" />
				</div>
			</Carousel>
		);
	}
}
export default DemoCarousel;
