import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { getProfiles } from "../../actions/profileActions";
import Spinner from "../spinner/Spinner";
import ProfileItem from "./ProfileItem";
import "../../CSS/partenaire.css";

class Profiles extends Component {
	componentDidMount = () => {
		console.log("get profiles");
		this.props.getProfiles();
	};
	render() {
		const {
			profile: { profiles, loading }
		} = this.props;
		return (
			<Fragment>
				{loading ? (
					<Spinner />
				) : (
					<Fragment>
						<h1 className="title_profile">Nos Partenaires</h1>
						<p className="sous_titre">Venez nous y rejoindre</p>
						<div className="row">
							{profiles.length > 0 ? (
								profiles.map(profile => (
									<ProfileItem key={profile._id} profile={profile} />
								))
							) : (
								<h4>Aucun profil trouvé...</h4>
							)}
						</div>
					</Fragment>
				)}
			</Fragment>
		);
	}
}

Profiles.propTypes = {
	getProfiles: propTypes.func.isRequired,
	profile: propTypes.object.isRequired
};

const mapStateToProps = state => ({
	profile: state.profile
});

export default connect(mapStateToProps, { getProfiles })(Profiles);
