import React from "react";
import { Link } from "react-router-dom";
import propTypes from "prop-types";
import "../../CSS/partenaire.css";

const ProfileItem = ({
	profile: {
		user: { _id, name, avatar },
		company,
		location,
		phone
	}
}) => {
	return (
		<div className=" column">
			<div className=" card_part ">
				<img src={avatar} alt="" className="round-img" />
				<div className="test">
					<p className="card_text">
						<i className="fa fa-fw fa-user "></i>
						<span className="icon_name">{name}</span>
					</p>
					<p className="card_text">
						<i className="fas fa-landmark"></i>
						<span className="icon_name">{company}</span>
					</p>
					<p className="card_text">
						<i className="fa fa-phone fa-flip-horizontal"></i>
						<span className="icon_name">{phone}</span>
					</p>
					<p className="card_text">
						{location && <i className="fas fa-map-marker-alt"></i>}
						<span className="icon_name">{location}</span>
					</p>
				</div>
				<Link to={`/profile/${_id}`}>
					<button className="button">Consulter le profil</button>
				</Link>
			</div>
		</div>
	);
};

ProfileItem.propTypes = {
	profile: propTypes.object.isRequired
};

export default ProfileItem;
