import React, { useEffect, Fragment } from "react";
import { Link } from "react-router-dom";
import propTypes from "prop-types";
import { connect } from "react-redux";
import DashboardAct from "./DashboardAct";
import { getCurrentProfile, deleteAccount } from "../../actions/profileActions";
import Spinner from "../spinner/Spinner";
import "../../CSS/dash.css";

const Dashboard = ({
	getCurrentProfile,
	deleteAccount,
	auth: { user },
	profile: { profile, loading }
}) => {
	useEffect(() => {
		getCurrentProfile();
	}, [getCurrentProfile]);
	return loading && profile === null ? (
		<Spinner />
	) : (
		<Fragment>
			<h1 className="text-dash">Dashboard</h1>
			<p>
				<i className="fas fa-user"></i> Bienvenue {user && user.name}
			</p>
			{profile !== null ? (
				<Fragment>
					<div className="try">
						<DashboardAct />
						<div>
							<button
								className="btn-danger btn-rem"
								onClick={() => deleteAccount()}>
								Supprimer Mon compte
							</button>
						</div>
					</div>
				</Fragment>
			) : (
				<Fragment>
					<p>
						{" "}
						Vous ne possédez pas de profil, remplissez le formulaire suivant
						pour créer le vôtre.{" "}
					</p>
					<Link to="/create-profile" className="btn btn-primary ">
						Crée mon profil
					</Link>
				</Fragment>
			)}
		</Fragment>
	);
};

Dashboard.propTypes = {
	getCurrentProfile: propTypes.func.isRequired,
	auth: propTypes.object.isRequired,
	profile: propTypes.object.isRequired,
	deleteAccount: propTypes.func.isRequired
};
const mapStateToProps = state => ({
	auth: state.auth,
	profile: state.profile
});
export default connect(mapStateToProps, { getCurrentProfile, deleteAccount })(
	Dashboard
);
