import React, { Component, Fragment } from "react";
import propTypes from "prop-types";
import { connect } from "react-redux";
import Spinner from "../spinner/Spinner";
import { getProfileById } from "../../actions/profileActions";
import { Link } from "react-router-dom";
import ProfileTop from "./ProfileTop";
import "../../CSS/profile.css";

class Profile extends Component {
	componentDidMount = () => {
		this.props.getProfileById(this.props.match.params.id);
	};
	render() {
		const {
			profile: { profile, loading },
			auth
		} = this.props;
		return (
			<Fragment>
				{profile === null || loading ? (
					<Spinner />
				) : (
					<Fragment>
						<Link to="/profiles" className="light-btn">
							{" "}
							Retour{" "}
						</Link>
						{auth.isAuthenticated &&
							auth.loading === false &&
							auth.user._id === profile.user._id && (
								<Link className="light-btn" to="/edit-profile">
									Edit Profile
								</Link>
							)}
						<div className="profile_grid">
							<ProfileTop profile={profile} />
						</div>
					</Fragment>
				)}
			</Fragment>
		);
	}
}

Profile.propType = {
	getProfileById: propTypes.func.isRequired,
	profile: propTypes.object.isRequired,
	auth: propTypes.object.isRequired
};
const mapStateToProps = state => ({
	profile: state.profile,
	auth: state.auth
});

export default connect(mapStateToProps, { getProfileById })(Profile);
