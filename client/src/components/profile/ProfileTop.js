import React from "react";
import propTypes from "prop-types";
import "../../CSS/profile.css";

const ProfileTop = ({
  profile: {
    bio,
    company,
    phone,
    website,
    social,
    user: { name, avatar }
  }
}) => {
  return (
    <div className="profile">
      <header className="profile_header">
        <img className="profile_avatar" src={avatar} alt="" />
        <h3 className="profile_name">{company}</h3>
      </header>

      <div className="para">
        <p>{bio}</p>
        <span className="icon_name">Nous contacter sur: {phone}</span>
      </div>

      <div className="profile_socails">
        {website && (
          <a
            className="profile_social"
            href={website}
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className=" fas fa-globe fa-2x"></i>
          </a>
        )}
        {social && social.twitter && (
          <a
            className="profile_social"
            href={social.twitter}
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className="fab fa-twitter fa-2x"></i>
          </a>
        )}
        {social && social.facebook && (
          <a
            className="profile_social"
            href={social.facebook}
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className="fab fa-facebook fa-2x"></i>
          </a>
        )}

        {social && social.linkedin && (
          <a
            className="profile_social"
            href={social.linkedin}
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className="fab fa-linkedin fa-2x"></i>
          </a>
        )}
        {social && social.instagram && (
          <a
            className="profile_social"
            href={social.instagram}
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className="fab fa-instagram fa-2x"></i>
          </a>
        )}
      </div>
    </div>
  );
};

ProfileTop.propTypes = {
  profile: propTypes.object.isRequired
};

export default ProfileTop;
