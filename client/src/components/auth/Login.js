import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { login } from "../../actions/authActions";
import "../../CSS/login.css";

class Login extends Component {
	constructor() {
		super();
		this.state = {
			email: "",
			password: ""
		};
	}

	onChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};
	onSubmit = async e => {
		e.preventDefault();
		const userData = {
			email: this.state.email,
			password: this.state.password
		};
		this.props.login(userData);
		// console.log(userData)
	};

	render() {
		if (this.props.isAuthenticated) {
			return <Redirect to="/listposts" />;
		}

		return (
			<div>
				<h3>Bon retour parmi nous</h3>
				<p className="text">
					Identifiez-vous pour rester au courant de ce qui se passe dans votre
					sphère professionnelle.
				</p>
				<p>
					<i className="fas fa-user " /> S'identifier
				</p>

				<form className="form" onSubmit={this.onSubmit}>
					<div className="form-group">
						<input
							className="input"
							type="email"
							placeholder="Adresse e-mail "
							name="email"
							value={this.state.email}
							onChange={this.onChange}
							required
						/>
					</div>
					<div className="form-group">
						<input
							className="input"
							type="password"
							placeholder="Mot de passe"
							name="password"
							value={this.state.password}
							onChange={this.onChange}
							// error={errors.password}
							minLength="6"
						/>
					</div>
					<input type="submit" className="btn " value="S'identifier" />
				</form>
				<p className="bottom-form">
					Vous n'avez pas de compte?{" "}
					<Link className="direction" to="/register">
						Inscrivez-vous
					</Link>
				</p>
			</div>
		);
	}
}

Login.propTypes = {
	login: propTypes.func.isRequired,
	isAuthenticated: propTypes.bool
};
const mapStateToProps = state => ({
	isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { login })(Login);
