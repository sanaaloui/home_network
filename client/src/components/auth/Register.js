import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { setAlert } from "../../actions/alertActions";
import { register } from "../../actions/authActions";
import propTypes from "prop-types";
import "../../CSS/login.css";

class Register extends Component {
	constructor() {
		super();
		this.state = {
			name: "",
			email: "",
			password: "",
			password2: ""
		};
	}

	onChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};
	onSubmit = async e => {
		e.preventDefault();
		if (this.state.password !== this.state.password2) {
			this.props.setAlert(" Votre mot de passe ne correspond pas", "danger");

			// console.log("Passwords do not match ");
		} else {
			const newUser = {
				name: this.state.name,
				email: this.state.email,
				password: this.state.password,
				password2: this.state.password2
			};
			this.props.register(newUser);

			// console.log(newUser);
		}
	};
	render() {
		if (this.props.isAuthenticated) {
			return <Redirect to="/dashboard" />;
		}

		return (
			<div>
				<p className="register-text">
					Bienvenue dans votre communauté professionnelle
				</p>
				<p>
					<i className="fas fa-user" /> Créer votre compte
				</p>
				<form className="form" onSubmit={this.onSubmit}>
					<div className="form-group">
						<input
							className="input"
							type="text"
							placeholder="Nom"
							name="name"
							value={this.state.name}
							onChange={this.onChange}
						/>
					</div>
					<div className="form-group">
						<input
							className="input"
							type="email"
							placeholder="Adresse e-mail"
							name="email"
							value={this.state.email}
							onChange={this.onChange}
						/>
					</div>
					<div className="form-group">
						<input
							className="input"
							type="password"
							placeholder="Mot de passe"
							name="password"
							value={this.state.password}
							onChange={this.onChange}
						/>
						<small className="form-text">
							Votre mot de passe doit contenir un minimum de 6 caractères
						</small>
					</div>
					<div className="form-group">
						<input
							className="input"
							type="password"
							placeholder="Confirmer votre mot de passe"
							name="password2"
							value={this.state.password2}
							onChange={this.onChange}
						/>
					</div>
					<input type="submit" className="btn " value="S’inscrire" />
				</form>
				<p className="inscrit">
					Déjà inscrit(e)?{" "}
					<Link className="direction" to="/login">
						S'identifier
					</Link>
				</p>
			</div>
		);
	}
}

Register.propTypes = {
	setAlert: propTypes.func.isRequired,
	register: propTypes.func.isRequired,
	isAuthenticated: propTypes.bool
};

// const mapDispatchToProps = dispatch => {
// 	return {
// 		setAlert: payload => {
// 			dispatch(setAlert(payload));
// 		},
// 		register: payload => {
// 			dispatch(register(payload));
// 		}
// 	};
// };

const mapStateToProps = state => ({
	isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { setAlert, register })(Register);
