import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { logout } from "../actions/authActions";
import "../CSS/navbar.css";
import logo from "../img/logo.png";

const NavBar = ({ auth: { isAuthenticated, loading }, logout }) => {
	const authLinks = (
		<ul>
			<li>
				<Link to="/profiles">Nos partenaires</Link>
			</li>
			<li>
				<Link to="/listPosts">Nos Biens</Link>
			</li>
			<li>
				<Link to="/dashboard">
					<i className="fas fa-user"></i>{" "}
					<span className="hide-sm">Profil</span>
				</Link>
			</li>
			<li>
				<a onClick={logout} href="#!">
					<i className="fas fa-sign-out-alt"></i>{" "}
					<span className="hide-sm">Se déconnecter</span>
				</a>
			</li>
		</ul>
	);
	const guestLinks = (
		<ul>
			<li>
				<Link to="/profiles">Nos partenaires</Link>
			</li>

			<li>
				<Link to="/register">Inscrivez-vous</Link>
			</li>
			<li>
				<Link to="/login">S'identifier</Link>
			</li>
		</ul>
	);
	return (
		<div>
			<nav className="navbar">
				<h1>
					<Link to="/">
						<img className="puce" src={logo} alt="" width="170" />
					</Link>
				</h1>

				{!loading && (
					<Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>
				)}
			</nav>
		</div>
	);
};

NavBar.propTypes = {
	logout: propTypes.func.isRequired,
	auth: propTypes.object.isRequired
};

const mapStateToProps = state => ({
	auth: state.auth
});

export default connect(mapStateToProps, { logout })(NavBar);
