import React, { Component, Fragment } from "react";
import { Link, withRouter } from "react-router-dom";
import propTypes from "prop-types";
import { connect } from "react-redux";
import { createProfile } from "../../actions/profileActions";

class CreateProfile extends Component {
	constructor() {
		super();
		this.state = {
			displaySocialInputs: false,
			status: "",
			company: "",
			phone: "",
			website: "",
			location: "",
			bio: "",
			instagram: "",
			facebook: "",
			linkedin: "",
			twitter: ""
		};
	}

	onSubmit = async e => {
		e.preventDefault();
		const formData = {
			status: this.state.status,
			company: this.state.company,
			phone: this.state.phone,
			website: this.state.website,
			location: this.state.location,
			instagram: this.state.instagram,
			facebook: this.state.facebook,
			twitter: this.state.twitter
		};
		this.props.createProfile(formData, this.props.history);
		// console.log(userData)
	};

	onChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	ToggleButton() {
		this.setState(currentState => ({
			displaySocialInputs: !currentState.displaySocialInputs
		}));
	}

	render() {
		return (
			<Fragment>
				<h1 className="large text-primary">Create Your Profile</h1>
				<p className="lead">
					<i className="fas fa-user"></i> Let's get some information to make
					your profile stand out
				</p>
				<small>* = required field</small>
				<form className="form" onSubmit={e => this.onSubmit(e)}>
					<div
						className="form-group"
						value={this.state.status}
						onChange={e => this.onChange(e)}>
						<select name="status">
							<option value="0">* Select Professional Status</option>
							<option value="Developer">Developer</option>
							<option value="Junior Developer">Junior Developer</option>

							<option value="Other">Other</option>
						</select>
					</div>
					<div className="form-group">
						<input
							type="text"
							placeholder="Company"
							name="company"
							value={this.state.company}
							onChange={e => this.onChange(e)}
						/>
						<small className="form-text">
							Could be your own company or one you work for
						</small>
					</div>
					<div className="form-group">
						<input
							type="text"
							placeholder="Website"
							name="website"
							value={this.state.website}
							onChange={e => this.onChange(e)}
						/>
						<small className="form-text">
							Could be your own or a company website
						</small>
					</div>
					<div className="form-group">
						<input
							type="text"
							placeholder="Phone"
							name="phone"
							value={this.state.phone}
							onChange={e => this.onChange(e)}
						/>
						<small className="form-text">
							City & state suggested (eg. Boston, MA)
						</small>
					</div>

					<div className="form-group">
						<textarea
							placeholder="A short bio of yourself"
							name="bio"
							value={this.state.bio}
							onChange={e => this.onChange(e)}></textarea>
						<small className="form-text">Tell us a little about yourself</small>
					</div>

					<div className="my-2">
						<button
							onClick={() => this.ToggleButton()}
							value={!this.state.displaySocialInputs}
							type="button"
							className="btn btn-light">
							Add Social Network Links
						</button>
						<span>Optional</span>
					</div>
					{this.state.displaySocialInputs && (
						<Fragment>
							<div className="form-group social-input">
								<i className="fab fa-twitter fa-2x"></i>
								<input
									type="text"
									placeholder="Twitter URL"
									name="twitter"
									value={this.state.twitter}
									onChange={e => this.onChange(e)}
								/>
							</div>

							<div className="form-group social-input">
								<i className="fab fa-facebook fa-2x"></i>
								<input
									type="text"
									placeholder="Facebook URL"
									name="facebook"
									value={this.state.facebook}
									onChange={e => this.onChange(e)}
								/>
							</div>

							<div className="form-group social-input">
								<i className="fab fa-linkedin fa-2x"></i>
								<input
									type="text"
									placeholder="Linkedin URL"
									name="linkedin"
									value={this.state.linkedin}
									onChange={e => this.onChange(e)}
								/>
							</div>

							<div className="form-group social-input">
								<i className="fab fa-instagram fa-2x"></i>
								<input
									type="text"
									placeholder="Instagram URL"
									name="instagram"
									value={this.state.instagram}
									onChange={e => this.onChange(e)}
								/>
							</div>
						</Fragment>
					)}

					<input type="submit" className="btn btn-primary my-1" />
					<Link className="btn btn-light my-1" to="/dashboard">
						Go Back
					</Link>
				</form>
			</Fragment>
		);
	}
}

CreateProfile.propTypes = {
	createProfile: propTypes.func.isRequired
};

export default connect(null, { createProfile })(withRouter(CreateProfile));
