import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./components/Home";
import NavBar from "./components/NavBar";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import Alert from "./components/Alert";
import store from "./store";
import Dashboard from "./components/dashboard/Dashboard";
import CreateProfile from "./components/profile-forms/CreateProfile";
import EditProfile from "./components/profile-forms/EditProfile";
import Profiles from "./components/profiles/Profiles";
import Profile from "./components/profile/Profile";
import PrivateRoute from "./components/private-route/PrivateRoute";
import ListOfCards from "./components/ListOfCards/ListOfCards";
import AddPost from "./components/Post/AddPost";
import Details from "./components/Details/Details";
//Redux
import setAuthToken from "./utils/setAuthToken";
import { loadUser } from "./actions/authActions";

import "./App.css";

if (localStorage.token) {
	setAuthToken(localStorage.token);
	// store.dispatch(loadUser());
}

const App = () => {
	useEffect(() => {
		store.dispatch(loadUser());
	}, []);

	return (
		<Router>
			<div>
				<NavBar />
				<Route exact path="/" component={Home} />
				<section className="container">
					<Alert />
					<Switch>
						<Route exact path="/register" component={Register} />
						<Route exact path="/login" component={Login} />
						<Route exact path="/profiles" component={Profiles} />
						<Route exact path="/profile/:id" component={Profile} />
						<PrivateRoute exact path="/dashboard" component={Dashboard} />
						<PrivateRoute
							exact
							path="/create-profile"
							component={CreateProfile}
						/>
						<PrivateRoute exact path="/edit-profile" component={EditProfile} />
						<PrivateRoute exact path="/listPosts" component={ListOfCards} />
						<PrivateRoute exact path="/AddPost" component={AddPost} />
						<PrivateRoute exact path="/Details/:id" component={Details} />
					</Switch>
				</section>
			</div>
		</Router>
	);
};

export default App;
