import React, { Component } from "react";
import { Button, Modal, ModalHeader, ModalFooter, ModalBody } from "reactstrap";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Formik } from "formik";
import { Form } from "react-formik-ui";
import axios from "axios";
import { Field } from "formik";

const mapDispatchToProps = dispatch => ({});
const mapStateToProps = state => ({
  userId: state && state.auth && state.auth.user && state.auth.user._id,
  token: state.auth.token
});

class ModalComponet extends Component {
  constructor(props) {
    super(props);
    console.log(props);

    this.state = {
      image: [],

      ...this.props.Bien
    };
  }

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.modal}
          toggle={this.props.toggle}
          className=""
        >
          <ModalHeader toggle={this.props.toggle}>
            Modifier cette annonce :
          </ModalHeader>
          <ModalBody>
            <Formik
              onSubmit={values => {
                let formData = new FormData();

                // TODO:  this should go to the action

                console.log("onSubmit", values);

                values.image.forEach(photo => {
                  formData.append("image", photo);
                });
                formData.append("name", values.name);
                formData.append("price", values.price);
                formData.append("title", values.title);
                formData.append("text", values.text);
                formData.append("userId", values.userId);
                formData.append("NbrPieces", values.NbrPieces);
                formData.append("type", values.type);
                formData.append("floorspace", values.floorspace);
                formData.append("location", values.location);
                formData.append("nature", values.nature);

                axios
                  .put("/api/posts/" + values._id, formData, {
                    headers: {
                      "Content-Type": "multipart/form-data",
                      Authorization: `Bearer ${this.props.token}`
                    }
                  })
                  .then(res => {
                    console.log(res.data);
                    // redirect to list page
                    this.props.history.push("/listposts");
                  })
                  .catch(err => {
                    console.error(err);
                  });
              }}
              initialValues={{
                userId: this.props.userId,
                ...this.props.Bien
              }}
              render={({
                values,
                isSubmitting,
                errors,
                handleSubmit,
                setFieldValue
              }) => (
                <Form onSubmit={e => e.preventDefault()}>
                  <div className="add-header">
                    <div>
                      <label>
                        {" "}
                        <strong className="add-titel">Nom :</strong>
                      </label>
                      <br />
                      <Field
                        type="text"
                        name="name"
                        value={values.name}
                        className="add-input"
                      />
                      <br />
                      <label>
                        <strong className="add-titel">Prix :</strong>
                      </label>
                      <br />
                      <Field
                        type="text"
                        name="price"
                        placeholder="Ajouter le prix de votre bien"
                        className="add-input"
                      />
                      <br />
                      <label>
                        <strong className="add-titel">Titre :</strong>
                      </label>
                      <br />
                      <Field type="text" name="title" className="add-input" />
                      <br />
                      <label>
                        <strong className="add-titel">Description :</strong>
                      </label>
                      <br />
                      <Field
                        type="text"
                        name="text"
                        placeholder="Ajouter une description de votre bien"
                        className="add-description"
                      />
                    </div>

                    <div>
                      <label>
                        <strong className="add-titel">Nombre de piéces:</strong>
                      </label>
                      <br />
                      <Field
                        placeholder="Ajouter le nombre de piéces de votre bien"
                        type="text"
                        name="NbrPieces"
                        className="add-input"
                      />
                      <br />
                      <label>
                        <strong className="add-titel">nature :</strong>
                      </label>
                      <br />
                      <Field
                        type="text"
                        name="nature"
                        placeholder="Ajouter la nature de votre bien"
                        className="add-input"
                      />

                      <br />
                      <label>
                        <strong className="add-titel">type :</strong>
                      </label>
                      <br />
                      <Field
                        type="text"
                        name="type"
                        placeholder="Ajouter le type de votre bien"
                        className="add-input"
                      />
                      <br />

                      <label>
                        <strong className="add-titel">Superficie :</strong>
                      </label>
                      <br />
                      <Field
                        type="text"
                        name="floorspace"
                        placeholder="Ajouter le superficie de votre bien"
                        className="add-input"
                      />
                      <br />

                      <label>
                        <strong className="add-titel">localisation :</strong>
                      </label>
                      <br />
                      <Field
                        type="text"
                        name="location"
                        placeholder="Ajouter la localisation de votre bien"
                        className="add-input"
                      />
                      <br />
                    </div>
                  </div>
                  <br />
                  <input
                    className="btn btn-secondary"
                    type="submit"
                    value="Modifiter"
                    onClick={handleSubmit}
                  />
                </Form>
              )}
            />
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.props.toggle}>
              Annuler
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ModalComponet));
