import React, { Component } from "react";
import Card from "../Card/Card";
import axios from "axios";
import "./ListOfCards.css";
import { Link } from "react-router-dom";

export default class ListOfCards extends Component {
  state = {
    Biens: [],
    filter: {
      NbrPieces: "",
      priceMin: "",
      priceMax: "",
      nature: "",
      type: ""
    }
  };

  getAllCards = () => {
    axios.get("/api/posts").then(res =>
      this.setState({
        Biens: res.data
      })
    );
  };

  componentDidMount() {
    this.getAllCards();
  }

  render() {
    return (
      <div className="dispalyList">
        <div className="filtre">
          <strong className="header-filtre">
            Trouvez l'appartement ou la maison de vos rêves, à vendre ou à
            louer!
          </strong>
          <br />
          <br />
          <p>Chambres min:</p>
          <div>
            <select
              name="NbrPieces"
              type="text"
              value={this.state.filter.NbrPieces}
              onChange={e => {
                this.setState({
                  filter: { ...this.state.filter, NbrPieces: e.target.value }
                });
              }}
            >
              <option value="">choisir une valeur</option>

              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
          <br />
          <p>Prix min:</p>
          <input
            type="number"
            name="priceMin"
            value={this.state.filter.priceMin}
            onChange={e => {
              this.setState({
                filter: { ...this.state.filter, priceMin: e.target.value }
              });
            }}
          />
          <br />
          <br />
          <p>Prix max:</p>
          <input
            type="number"
            name="priceMax"
            value={this.state.filter.priceMax}
            onChange={e => {
              this.setState({
                filter: { ...this.state.filter, priceMax: e.target.value }
              });
            }}
          />
          <br />
          <br />
          <p>Nature:</p>
          <div>
            <select
              name="nature"
              type="text"
              value={this.state.filter.nature}
              onChange={e => {
                this.setState({
                  filter: { ...this.state.filter, nature: e.target.value }
                });
              }}
            >
              <option value="">selectionner une valeur</option>
              <option value="Location">location</option>
              <option value="Vente">vente</option>
            </select>
          </div>
          <br />
          <p>Catégorie:</p>
          <div>
            <select
              name="type"
              type="text"
              value={this.state.filter.type}
              onChange={e => {
                this.setState({
                  filter: { ...this.state.filter, type: e.target.value }
                });
              }}
            >
              <option value="">selectionner une valeur</option>
              <option value="Maison">Maison</option>
              <option value="Appartement">Appartement</option>
              <option value="Villa">Villa</option>
              <option value=" Bureau"> Bureau</option>
            </select>
          </div>
        </div>

        <div className="list">
          <div>
            <div className="listCard">
              {this.state.Biens.filter(
                function(item) {
                  return Object.keys(this.state.filter).every(key => {
                    // Object.keys() extrait les clés d'un objet donné
                    if (this.state.filter[key] === "") {
                      return true;
                    }
                    if (key === "priceMin") {
                      return item.price >= this.state.filter[key];
                    }
                    if (key === "priceMax") {
                      return item.price <= this.state.filter[key];
                    }
                    if (key === "NbrPieces") {
                      return item[key] >= this.state.filter[key];
                    }
                    if (item[key] != this.state.filter[key]) return false;
                    return true;
                  });
                }.bind(this)
              ).map((el, key) => (
                <div>
                  <Card Bien={el} key={key} />
                </div>
              ))}
            </div>
          </div>

          <div className="addImg">
            <div className="imgArrierePlan">
              <img
                src="uploads/1578078183635-6WDfPT3v.png"
                className="img"
                alt=""
              />
            </div>
            <Link to="/AddPost">
              <button className="AjoutBien">
                <strong>Publier une annonce gratuite</strong>
              </button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
