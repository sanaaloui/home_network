import React from "react";
import "./Card.css";
import { Link } from "react-router-dom";

const Card = ({ Bien }) => {
  return (
    <div className="card">
      <div>
        <img
          src={
            Bien && Bien.image && Bien.image[0]
              ? "uploads/" + Bien.image[0]
              : "image-not-available.jpg"
          }
          className="image"
          alt=""
        />
        <Link to={`/Details/${Bien._id}`} className="post-button-detail"></Link>

        <h3 className="title-Post"> {Bien.title}</h3>
        <ul className="Post ">
          <li className="post-list-item">
            <p className="post-list-title">Chambre:</p>
            <h3 className="post-list-detail"> S+{Bien.NbrPieces}</h3>
          </li>
          <li className="post-list-item">
            <p className="post-list-title">Nature:</p>
            <h3 className="post-list-detail">{Bien.nature}</h3>
          </li>
          <li className="post-list-item">
            <p className="post-list-title">Localisation:</p>
            <h3 className="post-list-detail"> {Bien.location}</h3>
          </li>
          <li className="post-list-item">
            <p className="post-list-title">Type: </p>
            <h3 className="post-list-detail"> {Bien.type}</h3>
          </li>
          <li className="post-list-item">
            <p className="post-list-title">Prix: </p>
            <h3 className="post-list-Price"> {Bien.price} DT</h3>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Card;
