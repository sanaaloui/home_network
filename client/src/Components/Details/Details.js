import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import "./Details.css";
import ModalComponet from "../Modal/Modal";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators
} from "reactstrap";

class Détails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      user: { _id: "" },
      Bien: {
        user: "",
        image: [],
        NbrPieces: "",
        nature: "",
        location: "",
        type: "",
        price: "",
        text: "",
        floorspace: "",
        modal: false
      }
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  // Carousel
  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === this.state.Bien.image.length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === 0
        ? this.state.Bien.image.length - 1
        : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  deletePost = k => {
    console.log("delete", k);

    axios
      .delete("/api/posts/" + k)
      .then(res => {
        console.log("res", res);
        this.props.history.push("/listposts");
      })
      .catch(err => {});
  };
  // Edit post
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  // si il est le propriétaire du poste il peut modifier ou supprimer le bien
  isPostOwner() {
    try {
      if (this.state.Bien.user._id == this.props.userId) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      console.log(error);
      return false;
    }
  }
  componentDidMount() {
    const postId = this.props.match.params.id;
    axios
      .get("/api/posts/" + postId)
      .then(res => {
        console.log("res", res);

        this.setState({
          Bien: res.data
        });
      })
      .catch(err => {
        console.error(err);
      });
  }

  render() {
    const Bien = this.state.Bien;
    const { activeIndex } = this.state;
    const slides = this.state.Bien.image.map(item => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item}
        >
          <img src={"../uploads/" + item} alt="" />
        </CarouselItem>
      );
    });

    return (
      <div>
        <strong className="detail-header">
          Description du bien immobilier:
        </strong>

        <div className="title-detail">
          {" "}
          <strong> Nom de l'agent à contacter :</strong>{" "}
          <Link to={`/profile/${this.state.Bien.user._id}`}>
            {this.state.Bien.user.name}
          </Link>
        </div>
        <br />
        <Carousel
          activeIndex={activeIndex}
          next={this.next}
          previous={this.previous}
        >
          <CarouselIndicators
            items={this.state.Bien.image}
            activeIndex={activeIndex}
            onClickHandler={this.goToIndex}
          />
          {slides}
          <CarouselControl
            direction="prev"
            directionText="Previous"
            onClickHandler={this.previous}
          />
          <CarouselControl
            direction="next"
            directionText="Next"
            onClickHandler={this.next}
          />
        </Carousel>
        <ul>
          <li className="aligment">
            {/* <p className="title-detail description">
              <strong>L'agent à contacter:</strong>
            </p>
            <h3 className="detail"> {Bien}</h3> */}
          </li>

          <li className="aligment">
            <p className="title-detail description">
              <strong>Description de bien:</strong>
            </p>
            <h3 className="detail"> {Bien.text}</h3>
          </li>
          <li className="aligment">
            <p className="title-detail">
              <strong> Nombre de piéces:</strong>
            </p>
            <h3 className="detail">{Bien.NbrPieces}</h3>
          </li>
          <li className="aligment">
            <p className="title-detail">
              {" "}
              <strong>Nature:</strong>
            </p>
            <h3 className="detail">{Bien.nature}</h3>
          </li>
          <li className="aligment">
            <p className="title-detail">
              <strong> Localisation:</strong>
            </p>
            <h3 className="detail">{Bien.location}</h3>
          </li>
          <li className="aligment">
            <p className="title-detail">
              <strong>Catégorie:</strong>{" "}
            </p>
            <h3 className="detail">{Bien.type}</h3>
          </li>
          <li className="aligment">
            <p className="title-detail">
              {" "}
              <strong>Prix: </strong>{" "}
            </p>
            <h3 className="detail">{Bien.price} DT</h3>
          </li>
          <li className="aligment">
            <p className="title-detail">
              {" "}
              <strong>Superficie:</strong>
            </p>
            <h3 className="detail"> {Bien.floorspace} m²</h3>
          </li>
        </ul>

        {this.isPostOwner() ? (
          <div className="butt-footer">
            <button
              className="edit button-detail"
              onClick={() => this.toggle()}
            >
              Modifier cette annonce
            </button>

            <button
              onClick={() => this.deletePost(Bien._id)}
              className="delete button-detail"
            >
              Supprimer cette annonce
            </button>

            <ModalComponet
              Bien={this.state.Bien}
              modal={this.state.modal}
              toggle={this.toggle}
            />
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  userId: state && state.auth && state.auth.user && state.auth.user._id,
  token: state.auth.token
});
export default connect(mapStateToProps)(withRouter(Détails));
