import React, { Component } from "react";
import "./AddPost.css";
import { Formik } from "formik";
import { Form, DropZone } from "react-formik-ui";
import axios from "axios";
import { Field } from "formik";
import { notify } from "react-notify-toast";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

const mapStateToProps = state => ({
  userId: state && state.auth && state.auth.user && state.auth.user._id,
  token: state.auth.token
});

class AddPost extends Component {
  render() {
    return (
      <div className="addPost">
        <strong className="headerAdd">Ajouter un nouveau bien:</strong>

        <Formik
          onSubmit={values => {
            let formData = new FormData();
            console.log("onSubmit", values);
            values.image.forEach(photo => {
              formData.append("image", photo);
            });
            formData.append("name", values.name);
            formData.append("price", values.price);
            formData.append("title", values.title);
            formData.append("text", values.text);
            formData.append("userId", values.userId);
            formData.append("NbrPieces", values.NbrPieces);
            formData.append("type", values.type);
            formData.append("floorspace", values.floorspace);
            formData.append("location", values.location);
            formData.append("nature", values.nature);

            axios
              .post("/api/posts", formData, {
                headers: {
                  "Content-Type": "multipart/form-data"
                }
              })
              .then(res => {
                console.log(res.data);
                // redirect to list page
                this.props.history.push("/listposts");
              })
              .catch(err => {
                console.error(err);
              });
          }}
          initialValues={{
            userId: this.props.userId,
            text: "",
            name: "new post",
            title: "",
            image: [],
            price: "",
            location: "",
            type: "",
            nature: "",
            floorspace: "",
            NbrPieces: ""
          }}
          render={({
            values,
            isSubmitting,
            errors,
            handleSubmit,
            setFieldValue
          }) => (
            <Form onSubmit={e => e.preventDefault()}>
              <div className="add-header">
                <div>
                  <label>
                    {" "}
                    <strong className="add-titel">Nom :</strong>
                  </label>
                  <br />
                  <Field
                    type="text"
                    name="name"
                    value={values.name}
                    className="add-input"
                  />
                  <br />
                  <label>
                    <strong className="add-titel">Prix :</strong>
                  </label>
                  <br />
                  <Field
                    type="text"
                    name="price"
                    placeholder="Ajouter le prix de votre bien"
                    className="add-input"
                  />
                  <br />
                  <label>
                    <strong className="add-titel">Titre :</strong>
                  </label>
                  <br />
                  <Field type="text" name="title" className="add-input" />
                  <br />
                  <label>
                    <strong className="add-titel">Description :</strong>
                  </label>
                  <br />
                  <Field
                    type="text"
                    name="text"
                    placeholder="Ajouter une description de votre bien"
                    className="add-description"
                  />
                </div>

                <div>
                  <label>
                    <strong className="add-titel">Nombre de piéces:</strong>
                  </label>
                  <br />
                  <Field
                    placeholder="Ajouter le nombre de piéces de votre bien"
                    type="text"
                    name="NbrPieces"
                    className="add-input"
                  />
                  <br />
                  <label>
                    <strong className="add-titel">nature :</strong>
                  </label>
                  <br />
                  <Field
                    type="text"
                    name="nature"
                    placeholder="Ajouter la nature de votre bien"
                    className="add-input"
                  />

                  <br />
                  <label>
                    <strong className="add-titel">type :</strong>
                  </label>
                  <br />
                  <Field
                    type="text"
                    name="type"
                    placeholder="Ajouter le type de votre bien"
                    className="add-input"
                  />
                  <br />

                  <label>
                    <strong className="add-titel">Superficie :</strong>
                  </label>
                  <br />
                  <Field
                    type="text"
                    name="floorspace"
                    placeholder="Ajouter le superficie de votre bien"
                    className="add-input"
                  />
                  <br />

                  <label>
                    <strong className="add-titel">localisation :</strong>
                  </label>
                  <br />
                  <Field
                    type="text"
                    name="location"
                    placeholder="Ajouter la localisation de votre bien"
                    className="add-input"
                  />
                  <br />
                </div>
              </div>

              <DropZone
                label="image:"
                placeholder="Clicker ou glissez les images de votre bien ici"
                clearButtonText="Effacer"
                name="image"
                onDrop={acceptedFiles => {
                  if (acceptedFiles.length === 0) {
                    return;
                  } else if (acceptedFiles.length + values.image.length > 10) {
                    // here i am checking on the number of files
                    // here i am using react toaster to get some notifications. don't need to use it
                    return notify.show(
                      "Vous avez un maximum de 10 images",
                      "error"
                    );
                  } else {
                    //// TODO: handle duplicate images
                    setFieldValue("image", values.image.concat(acceptedFiles));
                  }
                }}
              />
              {values.image.length > 0 ? (
                <button
                  onClick={() => {
                    values.image = [];
                  }}
                >
                  Réinitialiser images
                </button>
              ) : (
                ""
              )}
              <br />
              <input type="submit" value="ajouter" onClick={handleSubmit} />
              <div> {/* <pre>{JSON.stringify(values)}</pre> */}</div>
            </Form>
          )}
        />
      </div>
    );
  }
}

export default connect(mapStateToProps)(withRouter(AddPost));
