const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");
const auth = require("../../middleware/auth");
const Post = require("../../models/Post");
const Profile = require("../../models/Profile");
const User = require("../../models/User");
var multer = require("multer");
const shortid = require("shortid");

var upload = multer({ dest: "uploads/" });
var storage = multer.diskStorage({
	destination: function(req, file, callback) {
		callback(null, "./uploads");
	},
	filename: function(req, file, callback) {
		callback(
			null,
			Date.now() +
				"-" +
				shortid.generate() +
				"." +
				file.originalname.split(".").pop()
		);
	}
});
var upload = multer({ storage: storage }).array("image", 10);

// @route           POST api/posts
// @description     Create a post
// @access          Private

router.post("/", auth, async (req, res) => {
	upload(req, res, async function(err) {
		console.log("req.body", req.files);

		let images = req.files ? req.files.map(file => file.filename) : [];
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		try {
			const user = await User.findById(req.userId).select("-password");

			const newPost = new Post({
				user: req.body.userId,
				name: req.body.name,
				title: req.body.title,
				image: images,
				price: req.body.price,
				text: req.body.text,

				NbrPieces: req.body.NbrPieces,
				nature: req.body.nature,
				location: req.body.location,
				type: req.body.type,
				floorspace: req.body.floorspace
			});

			const post = await newPost.save();
			res.json(post);
		} catch (err) {
			console.log(err.message);
			res.status(500).send("Server Error");
		}
	});
});

// @route           GET api/posts
// @description     Get all posts
// @access          Private

router.get(
	"/",
	// auth,
	async (req, res) => {
		try {
			const posts = await Post.find().sort({ date: -1 });
			res.json(posts);
		} catch (err) {
			console.error(err.message);
			res.status(500).send("Server Error");
		}
	}
);

// @route           GET api/posts/:id
// @description     Get post by ID
// @access          Private

router.get(
	"/:id",
	//  auth,
	async (req, res) => {
		try {
			const post = await Post.findById(req.params.id).populate("user");
			// we need to check if there is a post with that id
			if (!post) {
				return res.status(404).json({ msg: "Post not found" });
			}

			res.json(post);
		} catch (err) {
			console.log(err.message);
			if (err.kind === "ObjectId") {
				return res.status(404).json({ msg: "Post not found" });
			}
			res.status(500).send("Server Error");
		}
	}
);

// @route           DELETE api/posts/:id
// @description     Delete a post
// @access          Private

router.delete(
	"/:id",
	// auth,
	async (req, res) => {
		try {
			const post = await Post.findById(req.params.id);

			// post dont exist
			if (!post) {
				return res.status(404).json({ msg: "Post not found" });
			}

			//Check user
			// if (post.user.toString() !== req.user.id) {
			//   return res.status(401).json({ msg: "User not authorized" });
			// }

			await post.remove();

			res.json({ msg: "Post removed" });
		} catch (err) {
			console.error(err.message);
			if (err.kind === "ObjectId") {
				return res.status(404).json({ msg: "Post not found" });
			}
			res.status(500).send("Server Error");
		}
	}
);

// @route           PUT api/posts/:id
// @description     update a post
// @access          Private

router.put("/:id", auth, async (req, res) => {
	upload(req, res, async function(err) {
		try {
			let images = req.files ? req.files.map(file => file.filename) : [];

			const post = await Post.findOneAndUpdate(
				{ _id: req.params.id },
				req.body
			);
			res.json(post);
		} catch (err) {
			console.log(err.message);
			res.status(500).send("Server Error");
		}
	});
});

// @route           PUT api/posts/unlike/:id
// @description     Unlike a post
// @access          Private

router.put("/unlike/:id", auth, async (req, res) => {
	try {
		const post = await Post.findById(req.params.id);

		// Check if the post has already been like by this user
		if (
			post.likes.filter(like => like.user.toString() === req.user.id).length ===
			0
		) {
			return res.status(400).json({ msg: "Post has not yet been liked" });
		}
		// Get rempve index
		const removeIndex = post.likes
			.map(like => like.user.toString())
			.indexOf(req.user.id);

		post.likes.splice(removeIndex, 1);

		await post.save();

		res.json(post.likes);
	} catch (err) {
		console.log(err.message);
		res.status(500).send("Server Error");
	}
});

module.exports = router;
