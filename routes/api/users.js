const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const User = require("../../models/User");

// @route  POST api/users
// @description Register user
// @access Public

router.post(
	"/",
	[
		check("name", "Prénom est requis")
			.not()
			.isEmpty(),
		check("email", "Veuillez indiquer une adresse Email valide").isEmail(),
		check(
			"password",
			"Veuillez entrer un mot de passe valide (6 ou plus caractères)"
		).isLength({ min: 6 })
	],
	async (req, res) => {
		// Finds the validation errors in this request and wraps them in an object with handy functions
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		const { name, email, password } = req.body;
		try {
			// See if user exists
			let user = await User.findOne({ email });
			if (user) {
				return res.status(400).json({
					errors: [{ msg: "Un utilisateur avec un tel email existe deja" }]
				});
			}

			// Get users gravatar
			const avatar = gravatar.url(email, {
				s: "200",
				r: "pg",
				d: "mm"
			});
			user = new User({
				name,
				email,
				avatar,
				password
			});

			// Encrypt password using bcrypt
			const salt = await bcrypt.genSalt(10);
			user.password = await bcrypt.hash(password, salt);
			await user.save();

			// Return JWT: because in the frontend when a user registers i want them to get logged in right way

			const payload = {
				user: {
					id: user.id
				}
			};
			jwt.sign(
				payload,
				config.get("jwtSecret"),
				{ expiresIn: 360000 },
				(err, token) => {
					if (err) throw err;
					res.json({ token });
				}
			);
		} catch (err) {
			console.error(err.message);
			res.status(500).send("Server error");
		}
	}
);

module.exports = router;
