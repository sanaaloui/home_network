const mongoose = require("mongoose");

const ProfileSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "user" // reference to the User model
	},
	status: {
		type: String
	},
	company: {
		type: String,
		required: true
	},
	phone: {
		type: String,
		required: true
	},
	website: {
		type: String
	},
	location: {
		type: String
	},

	bio: {
		type: String
	},

	social: {
		instagram: {
			type: String
		},
		facebook: {
			type: String
		},
		linkedin: {
			type: String
		},
		twitter: {
			type: String
		}
	},
	date: {
		type: Date,
		default: Date.now
	}
});

module.exports = Profile = mongoose.model("profile", ProfileSchema);
