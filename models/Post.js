const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PostSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "user"
  },
  text: {
    type: String,
    required: true
  },
  name: {
    type: String
  },
  title: {
    type: String
  },
  image: {
    type: [String]
  },
  NbrPieces: {
    type: Number,
    required: true
  },
  floorspace: {
    type: Number
  },
  location: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  nature: {
    type: String,
    required: true
  },
  price: {
    type: Number
    // required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Post = mongoose.model("post", PostSchema);
